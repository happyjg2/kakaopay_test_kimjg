<h1>API</h1>
<hr/>
    <ul>
        <li>목차
            <ul>
                <li><a href="./README.md">소개</a></li>
                <li><a href="./README_project.md">프로젝트 구조 및 구성</a></li>
                <li><a href="./README_interface.md">인터페이스 정의서</a></li>
                <li><a href="./README_table.md">테이블 정의서</a></li>
            </ul>
         </li>
    </ul>
<hr/>

<h2>프로젝트 구조 및 구성</h2>

<h3>개발환경</h3>
<ul>
    <li>OS : MAC</li>
    <li>개발 툴 : IntelliJ IDEA 2019.3.3 (Ultimate Edition)</li>
    <li>DB : mysql  Ver 8.0.18 for osx10.15 on x86_64 (Homebrew)</li>
    <li>언어 : Kotlin</li>
    <li>Framework : Spring Boot 2.3.1</li>
    <li>WAS : Spring Boot Embedded Tomcat</li>
    <li>Build Tool : Gradle</li>
    <li>Library : <a href="./build.gradle.kts">build.gradle.kts</a> 참조</li>
</ul>
