


drop table sprinkle_participation;
drop table sprinkle_room;
drop table sprinkle_user;

-- 뿌리기 회원정보
CREATE TABLE sprinkle_user (
  user_id INT(11) NOT NULL AUTO_INCREMENT comment '사용자 구분값',
  assets INT(11) NOT NULL DEFAULT 999999999 comment '자산',
  create_date datetime NOT NULL comment '가입일',
  PRIMARY KEY (user_id)
) comment'뿌리기 회원정보';

-- 뿌리기 대화방 
CREATE TABLE sprinkle_room (
  room_id VARCHAR(3) NOT NULL comment '대화방 token',
  room_money INT NOT NULL comment '뿌리기 금액',
  user_cnt INT NOT NULL comment '뿌리기 인원수',
  user_id int(11) NOT NULL comment '등록자',
  create_date datetime NOT NULL comment '생성일',
  stop_yn char(1) NOT NULL DEFAULT 'N' comment '대화방종료 YN',
  PRIMARY KEY (room_id),
  FOREIGN KEY (user_id) REFERENCES sprinkle_user (user_id)
) comment'뿌리기 대화방';


-- 뿌리기 대화방 참여 정보
CREATE TABLE sprinkle_participation (
  seq int(11) NOT NULL AUTO_INCREMENT comment '참여정보 seq',
  room_id VARCHAR(3) NOT NULL comment '대화방 token',
  user_money INT NOT NULL comment '사용자별 할당 금액',
  user_id int(11) NULL comment '사용자',
  pp_date datetime NULL comment '참여날짜',
  PRIMARY KEY (seq),
  FOREIGN KEY (room_id) REFERENCES sprinkle_room (room_id),
  FOREIGN KEY (user_id) REFERENCES sprinkle_user (user_id)
) comment'뿌리기 대화방 참여 정보';


select * from sprinkle_participation;
select * from sprinkle_room;
select * from sprinkle_user;
