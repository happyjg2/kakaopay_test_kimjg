package com.kakaopay.kimjg.api.model


/**
 * 조회API 응답 VO
 * SprinkleRoomInfo.userList
 */
data class SprinkleRoomUserInfo(var userMoney: Int,
                                var userId: Int)
