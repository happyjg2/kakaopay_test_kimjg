package com.kakaopay.kimjg.api.model

import java.time.LocalDateTime

/**
 * 조회API 응답 VO
 */
data class SprinkleRoomInfo(var roomMakeDate: LocalDateTime,
                            var roomMoney: Int,
                            var userTotalMoney: Int? = null,
                            var userList: List<SprinkleRoomUserInfo>? = null)
