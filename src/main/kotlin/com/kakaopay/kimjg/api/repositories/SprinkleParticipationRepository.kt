package com.kakaopay.kimjg.api.repositories

import com.kakaopay.kimjg.api.entities.SprinkleParticipation
import com.kakaopay.kimjg.api.model.SprinkleRoomUserInfo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param
import javax.transaction.Transactional

/**
 * 뿌리기 참여 Repository
 */
interface SprinkleParticipationRepository: JpaRepository<SprinkleParticipation, Int> {
    fun findTopByRoomIdAndUserId(roomId:String, userId: Int? = null) : SprinkleParticipation?

    fun findByRoomIdAndUserIdIsNotNull(roomId:String) : List<SprinkleRoomUserInfo>;

    /**
     * 뿌리기 받기 사용자 매핑
     */
    @Transactional
    @Modifying
    @Query(value="update sprinkle_participation set user_id= :userId, pp_date=now() where room_id= :roomId and user_id is null order by seq desc limit 1",  nativeQuery = true)
    fun addUserId(@Param("userId") userId:Int, @Param("roomId") roomId:String);
}
