package com.kakaopay.kimjg.api.repositories

import com.kakaopay.kimjg.api.entities.SprinkleRoom
import org.springframework.data.jpa.repository.JpaRepository

/**
 * 뿌리기 방 Repository
 */
interface SprinkleRoomRepository : JpaRepository<SprinkleRoom, String> {
    fun findByUserIdAndRoomId(userId: Int, roomId: String): SprinkleRoom?
    // fun findall(): List<SprinkleRoom>;
}
