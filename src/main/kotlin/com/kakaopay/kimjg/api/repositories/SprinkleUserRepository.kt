package com.kakaopay.kimjg.api.repositories

import com.kakaopay.kimjg.api.entities.SprinkleUser
import lombok.Getter
import lombok.Setter
import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * 사용자 Repository
 */
interface SprinkleUserRepository: JpaRepository<SprinkleUser, Int> {
   // fun findall(): List<SprinkleUser>;
}
