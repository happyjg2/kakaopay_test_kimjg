package com.kakaopay.kimjg.api.module.sprinkle

import com.kakaopay.kimjg.api.common.exception.CheckException
import com.kakaopay.kimjg.api.common.message.*
import com.kakaopay.kimjg.api.common.util.CommonUtil
import com.kakaopay.kimjg.api.entities.SprinkleParticipation
import com.kakaopay.kimjg.api.entities.SprinkleRoom
import com.kakaopay.kimjg.api.entities.SprinkleUser
import com.kakaopay.kimjg.api.model.SprinkleRoomInfo
import com.kakaopay.kimjg.api.repositories.SprinkleParticipationRepository
import com.kakaopay.kimjg.api.repositories.SprinkleRoomRepository
import com.kakaopay.kimjg.api.repositories.SprinkleUserRepository
import com.kakaopay.kimjg.api.service.SprinkleParticipationService
import com.kakaopay.kimjg.api.service.SprinkleUserService
import lombok.extern.slf4j.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.time.Clock
import java.time.LocalDateTime
import java.time.ZoneId
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.random.Random

/**
 * 사용자 조회 API Controller
 *
 * @See
 * Introduction
    카카오페이에는 머니 뿌리기 기능이 있습니다.
    ● 사용자는 다수의 친구들이 있는 대화방에서 뿌릴 금액과 받아갈 대상의 숫자를
    입력하여 뿌리기 요청을 보낼 수 있습니다.
    ● 요청 시 자신의 잔액이 감소되고 대화방에는 뿌리기 메세지가 발송됩니다.
    ● 대화방에 있는 다른 사용자들은 위에 발송된 메세지를 클릭하여 금액을 무작위로
    받아가게 됩니다.

    이번 과제에서는 ​UI 및 메세지 영역은 제외한 간소화된 REST API를 구현하는 것이
    목표​입니다.

    요구 사항
    ● 뿌리기, 받기, 조회 기능을 수행하는 REST API 를 구현합니다.
        ○ 요청한 사용자의 식별값은 숫자 형태이며 "X-USER-ID" 라는 HTTP Header로
        전달됩니다.
        ○ 요청한 사용자가 속한 대화방의 식별값은 문자 형태이며 "X-ROOM-ID" 라는
        HTTP Header로 전달됩니다.
        ○ 모든 사용자는 뿌리기에 충분한 잔액을 보유하고 있다고 가정하여 별도로
        잔액에 관련된 체크는 하지 않습니다.
    ● 작성하신 어플리케이션이 다수의 서버에 다수의 인스턴스로 동작하더라도 기능에
    문제가 없도록 설계되어야 합니다.
    ● 각 기능 및 제약사항에 대한 단위테스트를 반드시 작성합니다.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1.0/user")
class UserController {
    val logger: Logger = LoggerFactory.getLogger(UserController::class.java)

    @Autowired
    lateinit var sprinkleRoomRepository: SprinkleRoomRepository;
    @Autowired
    lateinit var sprinkleUserRepository: SprinkleUserRepository;
    @Autowired
    lateinit var sprinkleParticipationRepository: SprinkleParticipationRepository;

    @Autowired
    lateinit var sprinkleUserService: SprinkleUserService;

    @Autowired
    lateinit var sprinkleParticipationService: SprinkleParticipationService;

    /** 뿌리기 API
     * [require]
        ● 다음 조건을 만족하는 뿌리기 API를 만들어 주세요.
            ○ 뿌릴 금액, 뿌릴 인원을 요청값으로 받습니다.
            ○ 뿌리기 요청건에 대한 고유 token을 발급하고 응답값으로 내려줍니다.
            ○ 뿌릴 금액을 인원수에 맞게 분배하여 저장합니다. (분배 로직은 자유롭게 구현해 주세요.)
            ○ token은 3자리 문자열로 구성되며 예측이 불가능해야 합니다.
     * @see
     * URI : /api/v1.0/user/sprinkle
     * Content-Type : application/x-www-form-urlencoded
     * Accept : application/json
     * Method : POST
     * @param money : 뿌릴 금액 (필수)
     * @param userCnt : 뿌릴 인원 (필수)
     * @request.header.X-USER-ID : 사용자ID
     * [noti]
        header에서 사용자 정보를 조회합니다.
        ○ 사용자 정보가 없으면, 사용자 생성 후 뿌리기 방을 생성 합니다.
        ○ 사용자 정보가 있으면, 해당 사용자의 뿌리기 방을 생성 합니다.
        ○ 뿌리기 방을 생성 후 요청한 금액과 인원 수에 맞게 뿌리기 참여방이 생성 됩니다.
        ○ 뿌리기 참여방에는 분배금액이 할당 되어 있고, 분배 금액은 최소 1원 ~ 랜덤하게 뿌려져 있습니다.
        ○ 뿌리기 참여방이 완료되면 사용자의 잔액이 차감되나, '별도로 잔액에 관련된 체크는 하지 않습니다.' 규약에 따라 더 이상의 검사는 하지 않습니다.
     * @return data = 뿌리기방 ID
     */
    @PostMapping("sprinkle")
    fun sprinkle(responseMessage: ResponseMessage
                 , request: HttpServletRequest
                 , response: HttpServletResponse
                 , @RequestHeader(Const.USER_ID) userId:String?
                 , @RequestParam(value = "money", defaultValue = "0") money:Int
                 , @RequestParam(value = "userCnt", defaultValue = "0") userCnt:Int
                 , @RequestParam reqMap:HashMap<String, Any>? ): ResponseMessage {
        logger.debug("start-date-time = {}", LocalDateTime.now())
        logger.debug("list param={}",reqMap)

        // ---------------------------------------------------
        // 요청값 체크
        // ---------------------------------------------------
        if(money == 0) {
            throw CheckException(ErrorCode.E401); //"뿌리기 금액 요청값이 없습니다."
        }

        if(userCnt == 0) {
            throw CheckException(ErrorCode.E402); // "뿌리기 인원 요청값이 없습니다."
        }

        if(money < userCnt) {
            throw CheckException(ErrorCode.E403); // "뿌리기 금액은 뿌리기 인원보다 많아야 합니다."
        }

        // ---------------------------------------------------
        // 비즈니스 로직 시작
        // ---------------------------------------------------
        // 사용자 확인 및 정보 저장
        var sprinkleUser = sprinkleUserService.getUserInfo(userId);
        response.setHeader(Const.USER_ID, sprinkleUser.userId.toString());

        // 뿌리기 대화방 생성
        var roomId = CommonUtil.randomTokenId();
        var index = 0;
        while(sprinkleRoomRepository.findById(roomId).isPresent) {
            roomId = CommonUtil.randomTokenId();
            logger.debug("make($index) roomId = $roomId");
            index++;
            if(index > 100) {
                // 100회 조회 시, 오류 메세지 출력
                throw CheckException(ErrorCode.E500); // "대화방을 생성 할 수 없습니다. 문제가 지속되면 관리자에게 문의 바랍니다."
            }
        }
        var sprinkleRoom = SprinkleRoom(roomId, money, userCnt, sprinkleUser.userId, Const.STOP_N, LocalDateTime.now())
        sprinkleRoomRepository.save(sprinkleRoom);

        // 뿌리기 참여방 생성
        sprinkleParticipationService.make(roomId, money, userCnt)

        // 내 자신에서 뿌린 금액 제외
        sprinkleUser.assets -= money
        sprinkleUserRepository.save(sprinkleUser);

        // ---------------------------------------------------
        // 응답값 설정
        // ---------------------------------------------------
        responseMessage.result = Result.SUCCESS;
        responseMessage.data = roomId;

        response.setHeader(Const.ROOM_ID, roomId)
        return responseMessage;
    }

    /** 조회 API
     * [require]
        ● 다음 조건을 만족하는 조회 API를 만들어 주세요.
            ○ 뿌리기 시 발급된 token을 요청값으로 받습니다.
            ○ token에 해당하는 뿌리기 건의 현재 상태를 응답값으로 내려줍니다. 현재 상태는 다음의 정보를 포함합니다.
            ○ 뿌린시각,뿌린금액,받기완료된금액,받기완료된정보([받은금액,받은 사용자 아이디] 리스트)
            ○ 뿌린 사람 자신만 조회를 할 수 있습니다. 다른사람의 뿌리기건이나 유효하지 않은 token에 대해서는 조회 실패 응답이 내려가야 합니다.
            ○ 뿌린건에대한조회는7일동안할수있습니다.
     * @see
     * URI : /api/v1.0/user/sprinkle
     * Content-Type : application/x-www-form-urlencoded
     * Accept : application/json
     * Method : GET
     * @request.header.X-USER-ID : 사용자ID (필수)
     * @request.header.X-ROOM-ID : 룸ID (필수)
     * [noti]
        ○ require와 같음
     * @return data 뿌리기방 정보
     */
    @GetMapping("sprinkle")
    fun sprinkle(responseMessage: ResponseMessage
                 , request: HttpServletRequest
                 , response: HttpServletResponse
                 , @RequestHeader(Const.USER_ID) userId:String?
                 , @RequestHeader(Const.ROOM_ID) roomId:String?
                 , @RequestParam reqMap:HashMap<String, Any>?  ): ResponseMessage {
        logger.debug("start-date-time = {}", LocalDateTime.now())
        logger.debug("search reqMap={}",reqMap)

        // ---------------------------------------------------
        // 요청값 체크
        // ---------------------------------------------------
        if(userId.isNullOrEmpty()) {
            throw CheckException(ErrorCode.E103) // "사용자 정보가 없습니다."
        } else if(!CommonUtil.isInteger(userId)){
            throw CheckException(ErrorCode.E100); // "잘못된 사용자 요청 입니다."
        }

        if(roomId.isNullOrEmpty()) {
            throw CheckException(ErrorCode.E200); // "대화방 정보가 없습니다."
        }

        // 헤더 설정
        response.setHeader(Const.USER_ID, userId);
        response.setHeader(Const.ROOM_ID, roomId);

        // ---------------------------------------------------
        // 비즈니스 로직 시작
        // ---------------------------------------------------
        // 대화방 정보 조회
        val sprinkleRoom = sprinkleRoomRepository.findByUserIdAndRoomId(userId.toInt(), roomId)
                ?: throw CheckException(ErrorCode.E201); // "요청한 사용자의 대화방 정보가 없습니다."

        // 대화방 조회 시간 체크
        if (LocalDateTime.now().isAfter(sprinkleRoom.createDate.plusDays(7))) {
            throw CheckException(ErrorCode.E202) // "조회 시간이 지났습니다."
        }

        // 대화방 정보 저장
        val sprinkleRoomInfo = SprinkleRoomInfo(sprinkleRoom.createDate, sprinkleRoom.roomMoney);
        sprinkleRoomInfo.userList = sprinkleParticipationRepository.findByRoomIdAndUserIdIsNotNull(roomId);
        sprinkleRoomInfo.userTotalMoney = sprinkleRoomInfo.userList!!.sumBy { it.userMoney }

        // ---------------------------------------------------
        // 응답값 설정
        // ---------------------------------------------------
        responseMessage.result = Result.SUCCESS;
        responseMessage.data = sprinkleRoomInfo;

        return responseMessage;
    }

    /** 받기 API
     * [require]
        ● 다음 조건을 만족하는 받기 API를 만들어 주세요.
            ○ 뿌리기 시 발급된 token을 요청값으로 받습니다.
            ○ token에 해당하는 뿌리기 건 중 아직 누구에게도 할당되지 않은 분배건 하나를 API를 호출한 사용자에게 할당하고, 그 금액을 응답값으로 내려줍니다.
            ○ 뿌리기당 한 사용자는 한 번 만받을 수 있습니다.
            ○ 자신이 뿌리기한 건은 자신이 받을 수 없습니다.
            ○ 뿌린기가 호출된 대화방과 동일한 대화방에 속한 사용자만이 받을 수 있습니다.
            ○ 뿌린 건은 10분간만 유효합니다. 뿌린지 10분이 지난 요청에 대해서는 받기 실패 응답이 내려가야 합니다.
     * @see
     * URI : /api/v1.0/user/receive
     * Content-Type : application/x-www-form-urlencoded
     * Accept : application/json
     * Method : POST
     * @request.header.X-USER-ID : 사용자ID
     * @request.header.X-ROOM-ID : 룸ID (필수)
     * [noti]
        ○ 사용자 정보가 없으면, 사용자 생성 후 받기를 시작합니다.
        ○ 사용자 정보가 있으면, 받기를 시작합니다.
        ○ require와 같음
        ○ 받은 금액을 자산에 포함 시키지는 않았음 - 요구사항에 없음.
     * @return data 받은 금액
     */
    @PostMapping("receive")
    fun receive(responseMessage: ResponseMessage
             , request: HttpServletRequest
             , response: HttpServletResponse
             , @RequestHeader(Const.USER_ID) userId:String?
             , @RequestHeader(Const.ROOM_ID) roomId:String?
             , @RequestParam reqMap:HashMap<String, Any>? ): ResponseMessage {
        logger.debug("start-date-time = {}", LocalDateTime.now())
        logger.debug("receive $userId, $roomId param={}",reqMap)

        // ---------------------------------------------------
        // 요청값 체크
        // ---------------------------------------------------
        if(roomId.isNullOrEmpty()) {
            throw CheckException(ErrorCode.E200);// "대화방 정보가 없습니다."
        }

        // ---------------------------------------------------
        // 비즈니스 로직 시작
        // ---------------------------------------------------
        // 사용자 정보 확인 및 생성
        var sprinkleUser = sprinkleUserService.getUserInfo(userId);

        // 헤더 설정
        response.setHeader(Const.USER_ID, sprinkleUser.userId.toString());
        response.setHeader(Const.ROOM_ID, roomId);

        // 대화방 뿌리가 참여 확인 및 정보조회
        var sprinkleParticipation =  sprinkleParticipationService.getParticipationInfo(roomId, sprinkleUser);

        // 대화방 뿌리기 금액 받기
        sprinkleParticipationRepository.addUserId(sprinkleUser.userId, roomId);
        //sprinkleParticipation = sprinkleParticipationRepository.save(sprinkleParticipation);

        // 대화방 뿌리기 금액 확인
        var result: SprinkleParticipation? = sprinkleParticipationRepository.findTopByRoomIdAndUserId(roomId, sprinkleUser.userId);
        if(result == null) {
            throw CheckException(ErrorCode.E307); // "사용자가 많습니다. 잠시 후 다시 시도해 주세요."
        }

        responseMessage.result = Result.SUCCESS;
        responseMessage.data = "" + result.userMoney + " 원" // 금액 전달

        return responseMessage;
    }


}
