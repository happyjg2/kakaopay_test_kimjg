package com.kakaopay.kimjg.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

/**
 * spring boot 실행
 */
@SpringBootApplication
open class ApiApplication: SpringBootServletInitializer() {

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(ApiApplication::class.java)
    }
}

fun main(args: Array<String>) {
    runApplication<ApiApplication>(*args)
}
