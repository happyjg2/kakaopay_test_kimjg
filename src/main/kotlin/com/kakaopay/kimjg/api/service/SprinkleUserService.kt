package com.kakaopay.kimjg.api.service

import com.kakaopay.kimjg.api.common.exception.CheckException
import com.kakaopay.kimjg.api.common.message.Const
import com.kakaopay.kimjg.api.common.message.ErrorCode
import com.kakaopay.kimjg.api.common.util.CommonUtil
import com.kakaopay.kimjg.api.entities.SprinkleUser
import com.kakaopay.kimjg.api.repositories.SprinkleUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime

/**
 * 사용자 Service
 */
@Service
class SprinkleUserService {
    @Autowired
    lateinit var sprinkleUserRepository: SprinkleUserRepository;

    /**
     * 사용자 정보 확인 및 생성
     *
     * @see
     * request param에 사용자가 없으면, 회원가입 처리
     * request param에 사용자가 있으면, 해당 회원정보 보여줌
     */
    fun getUserInfo(userId:String?) : SprinkleUser {
        var sprinkleUser: SprinkleUser
        if(userId.isNullOrEmpty()) {
            // 헤더의 사용자 정보가 없으면, 회원가입
            sprinkleUser = SprinkleUser(Const.ASSETS, LocalDateTime.now());
            sprinkleUser = sprinkleUserRepository.save(sprinkleUser)
        } else if(!CommonUtil.isInteger(userId)){
            throw CheckException(ErrorCode.E100); // "잘못된 사용자 요청 입니다."
        } else {
            var entity = sprinkleUserRepository.findById(userId.toInt());

            if(!entity.isPresent){
                throw CheckException(ErrorCode.E101) // "잘못된 사용자 요청 입니다."
            }
            sprinkleUser = entity.get();
        }

        return sprinkleUser;
    }
}
