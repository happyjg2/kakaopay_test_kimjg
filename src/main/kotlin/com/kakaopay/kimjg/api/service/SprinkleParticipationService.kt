package com.kakaopay.kimjg.api.service

import com.kakaopay.kimjg.api.common.exception.CheckException
import com.kakaopay.kimjg.api.common.message.Const
import com.kakaopay.kimjg.api.common.message.ErrorCode
import com.kakaopay.kimjg.api.entities.SprinkleParticipation
import com.kakaopay.kimjg.api.entities.SprinkleUser
import com.kakaopay.kimjg.api.repositories.SprinkleParticipationRepository
import com.kakaopay.kimjg.api.repositories.SprinkleRoomRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import kotlin.random.Random

/**
 * 뿌리기 참여 Service
 */
@Service
class SprinkleParticipationService {
    @Autowired
    lateinit var sprinkleParticipationRepository: SprinkleParticipationRepository;

    @Autowired
    lateinit var sprinkleRoomRepository: SprinkleRoomRepository;

    /**
     * 뿌리기 참여방 금액 별 분배 처리 후 생성
     *
     * @see
     * 뿌리기 방이 생성이 되면,
     * 사용자가 입력한 뿌리기 금액을 랜덤하게 분배하여 뿌리기 인원 수 만큼 뿌리기 참여 record가 생성이 된다.
     * 뿌리기 참여 record에 사용자ID가 매핑 되었으면, 사용자 받기 완료 상태이다.
     */
    fun make(roomId: String, money: Int, userCnt: Int) {
        var money = money;
        var userCnt = userCnt;
        var sum = 0;
        var userMoney = 0;
        var list = mutableListOf<Int>()
        for(i in 1..userCnt){
            if(i == userCnt){
                userMoney = money;
            } else {
                userMoney = Random.nextInt(0, (money - (userCnt - i ))) + 1
                money -= userMoney;
            }
           // logger.debug("[sprinkle-set] 분배시작 : $i 명 =  $userMoney --> remain : $money")
            list.add(userMoney);

            sum+= userMoney;
        }

        list.shuffle();
        for(i in 1..list.size) {
            var sprinkleParticipation = SprinkleParticipation(roomId, list[i-1])
            sprinkleParticipationRepository.save(sprinkleParticipation)
        }

        // logger.debug("[sprinkle-set] 총 $userCnt 에게 $sum 금액을 분배 하였습니다. {}", sum == money)
    }

    /**
     * 뿌리기 참여 가능 확인
     */
    fun getParticipationInfo(roomId:String, sprinkleUser: SprinkleUser): SprinkleParticipation {
        // 대화방 확인
        val entity =  sprinkleRoomRepository.findById(roomId);
        if(!entity.isPresent) {
            throw CheckException(ErrorCode.E301);// "잘못된 대화방 요청 입니다."
        }

        // 대화방 종료 여부 체크
        val sprinkleRoom = entity.get();
        if(sprinkleRoom.userId == sprinkleUser.userId) {
            throw CheckException(ErrorCode.E302); // "소유자는 참여 할 수 없습니다."
        }

        if(sprinkleRoom.stopYn == Const.STOP_Y) {
            throw CheckException(ErrorCode.E303) // "종료 된 대화방 입니다."
        }

        // 대화방 참여 시간 체크
        if (LocalDateTime.now().isAfter(sprinkleRoom.createDate.plusMinutes(10))) {
            sprinkleRoom.stopYn = Const.STOP_Y;
            sprinkleRoomRepository.save(sprinkleRoom)
            throw CheckException(ErrorCode.E304); // "참여 시간이 종료되었습니다."
        }

        // 중복 사용자 체크
        if(sprinkleParticipationRepository.findTopByRoomIdAndUserId(roomId, sprinkleUser.userId) != null) {
            throw CheckException(ErrorCode.E305); // "이미 참여 하였습니다."
        }

        // 대화방 뿌리기 참여 가능 확인
        var sprinkleParticipation: SprinkleParticipation? = sprinkleParticipationRepository.findTopByRoomIdAndUserId(roomId);
        if(sprinkleParticipation == null) {
            // 없으면 종료 처리 후 알림
            sprinkleRoom.stopYn = Const.STOP_Y;
            sprinkleRoomRepository.save(sprinkleRoom)
            throw CheckException(ErrorCode.E306); // "뿌리기 이벤트가 종료되었습니다."
        }

        return sprinkleParticipation;
    }
}
