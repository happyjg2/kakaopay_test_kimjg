package com.kakaopay.kimjg.api.entities

import lombok.Getter
import lombok.Setter
import java.time.LocalDateTime
import javax.persistence.*

/**
 * 뿌리기 사용자 Entity
 *
-- 뿌리기 회원정보
CREATE TABLE sprinkle_user (
user_id INT(11) NOT NULL AUTO_INCREMENT comment '사용자 구분값',
assets INT(11) NOT NULL DEFAULT 999999999 comment '자산',
create_date datetime NOT NULL comment '가입일',
PRIMARY KEY (user_id)
) comment'뿌리기 회원정보';
 */
@Getter
@Setter
@Entity
@Table
data class SprinkleUser (var assets: Int,
                         var createDate: LocalDateTime){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var userId: Int = 0;
}
