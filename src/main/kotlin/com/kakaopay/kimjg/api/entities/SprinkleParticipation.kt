package com.kakaopay.kimjg.api.entities

import lombok.Getter
import lombok.Setter
import java.time.LocalDateTime
import javax.persistence.*

/**
 * 뿌리기 대화방 참여 정보 Entity
 *
-- 뿌리기 대화방 참여 정보
CREATE TABLE sprinkle_participation (
seq int(11) NOT NULL AUTO_INCREMENT comment '참여정보 seq',
room_id VARCHAR(3) NOT NULL comment '대화방 token',
user_money INT NOT NULL comment '사용자별 할당 금액',
user_id int(11) NULL comment '사용자',
pp_date datetime NULL comment '참여날짜',
PRIMARY KEY (seq),
FOREIGN KEY (room_id) REFERENCES sprinkle_room (room_id),
FOREIGN KEY (user_id) REFERENCES sprinkle_user (user_id)
) comment'뿌리기 대화방 참여 정보';
 */
@Getter
@Setter
@Entity
@Table
data class SprinkleParticipation (var roomId: String,
                                  var userMoney: Int,
                                  var userId: Int? = null,
                                  var ppDate: LocalDateTime? = null){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var seq: Int = 0;
}
