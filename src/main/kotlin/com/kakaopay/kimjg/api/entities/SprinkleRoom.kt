package com.kakaopay.kimjg.api.entities

import com.kakaopay.kimjg.api.common.message.Const
import lombok.Getter
import lombok.Setter
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * 뿌리기 대화방 Entity
 *
-- 뿌리기 대화방
CREATE TABLE sprinkle_room (
room_id VARCHAR(3) NOT NULL comment '대화방 token',
room_money INT NOT NULL comment '뿌리기 금액',
user_cnt INT NOT NULL comment '뿌리기 인원수',
user_id int(11) NOT NULL comment '등록자',
create_date datetime NOT NULL comment '생성일',
stop_yn char(1) NOT NULL DEFAULT 'N' comment '대화방종료 YN',
PRIMARY KEY (room_id),
FOREIGN KEY (user_id) REFERENCES sprinkle_user (user_id)
) comment'뿌리기 대화방';
 */
@Getter
@Setter
@Entity
@Table
data class SprinkleRoom(@Id
                        var roomId: String,
                        var roomMoney: Int,
                        var userCnt: Int,
                        var userId: Int,
                        var stopYn: Char,
                        var createDate: LocalDateTime)
