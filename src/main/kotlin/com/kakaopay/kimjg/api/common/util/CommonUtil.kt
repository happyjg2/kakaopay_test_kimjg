package com.kakaopay.kimjg.api.common.util

import kotlin.random.Random

/**
 * 공통 Util
 */
object CommonUtil {

    fun randomTokenId(): String{
        return randomChar().plus(randomChar()).plus(randomChar());
    }

    fun randomChar():String {
        return  Random.nextInt(65,90).toChar().toString();
    }
    
    fun isInteger(str: String?) = str?.toIntOrNull()?.let { true } ?: false
}
