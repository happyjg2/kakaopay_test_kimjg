package com.kakaopay.kimjg.api.common.message

/**
 * 상수값 정의
 */
object Const {
    const val USER_ID = "X-USER-ID"
    const val ROOM_ID = "X-ROOM-ID"
    const val ASSETS = 1000000000;

    const val STOP_Y = 'Y';
    const val STOP_N = 'N';
}
