package com.kakaopay.kimjg.api.common.exception

import com.kakaopay.kimjg.api.common.message.ResponseMessage
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.NoHandlerFoundException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.NullPointerException

/**
 * Exception 처리
 */
@ControllerAdvice
@RestController
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = [(CheckException::class)])
    fun handleConflict(exception: CheckException, request: WebRequest): ResponseMessage {
        println("Handle")
        val responseMessage = ResponseMessage();
        responseMessage.data = exception.message + " ("+exception.code+")"
        return responseMessage;
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = [(Throwable::class)])
    fun handleConflictThrow(exception: Throwable, request: WebRequest): ResponseMessage {
        println("Handle")
        val responseMessage = ResponseMessage();
        responseMessage.data = "알 수 없는 오류 입니다.";
        return responseMessage;
    }

//    @ResponseStatus(HttpStatus.OK)
//    @ExceptionHandler(value = [(NoHandlerFoundException::class)])
//    fun handleConflict404(exception: NoHandlerFoundException, request: WebRequest): ResponseMessage {
//        println("Handle")
//        val responseMessage = ResponseMessage();
//        responseMessage.data = "페이지를 잘 못 호출 하였습니다.";
//        return responseMessage;
//    }

}
