package com.kakaopay.kimjg.api.common.config

import com.kakaopay.kimjg.api.common.interceptor.WebInterceptor
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.*

@Configuration
open class WebConfiguration : WebMvcConfigurer {

    override fun addCorsMappings(registry: CorsRegistry){
        registry.addMapping("/api/**/*")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("GET","POST","DELETE","PUT")
                .allowCredentials(true)
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/")
    }

    override fun configureDefaultServletHandling(configurer: DefaultServletHandlerConfigurer) {
        configurer.enable();
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(WebInterceptor());
    }
}
