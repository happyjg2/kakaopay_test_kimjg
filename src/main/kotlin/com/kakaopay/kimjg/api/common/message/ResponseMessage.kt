package com.kakaopay.kimjg.api.common.message

import lombok.Getter
import lombok.Setter
import java.time.LocalDateTime

/**
 * Response Body 공통 규격
 *
 * result : 성공, 실패
 * data : 비즈니스 로직 결과
 * serverTime : 서버 시간
 */
@Getter
@Setter
data class ResponseMessage(var result:Result = Result.FAIL) {
    var serverTime: LocalDateTime = LocalDateTime.now()
    var data:Any? = null
}

enum class Result {
    SUCCESS, FAIL
}
