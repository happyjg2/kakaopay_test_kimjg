package com.kakaopay.kimjg.api.common.exception

import com.kakaopay.kimjg.api.common.message.ErrorCode
import java.lang.Exception

/**
 * 비즈니스 로직 체크 Exception
 */
class CheckException(errorCode:ErrorCode): Exception(errorCode.message) {
    val code:Int = errorCode.code

}
