package com.kakaopay.kimjg.api.module

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import kotlin.random.Random

class Test{

    /**
     * 뿌리기금액 사용자별 분배 - 동일 테스트
     */
    @Test
    fun testSprinkleParticipationAdd() {
        val money = 1000;
        val userCnt = 3;

        for(i in 1..userCnt){
            System.out.println("1명 = " + money / userCnt)
        }

    }

    /**
     * 뿌리기금액 사용자별 분배 - 랜덤 테스트
     */
    @Test
    fun testSprinkleParticipationAdd_random() {

        var money = 2000;
        val userCnt = 1000;

        var sum = 0;
        var userMoney = 0;
        for(i in 1..userCnt){
            if(i == userCnt){
                userMoney = money;
            } else {
                userMoney = Random.nextInt(0, (money - (userCnt - i ))) + 1
                money -= userMoney;
            }
            System.out.println( "$i 명 =  $userMoney --> remain : $money")
            sum+= userMoney;

        }
        System.out.println( "$sum")
    }

}
