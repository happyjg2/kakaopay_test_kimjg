package com.kakaopay.kimjg.api.module

import com.kakaopay.kimjg.api.common.BaseTest
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.util.stream.IntStream

/**
 * 전체 API TEST
 * Content-Type : application/x-www-form-urlencoded
 * Accept : application/json
 * Method : GET
 */
class SprinkleAllTest : BaseTest() {
    /**
     * 전체 API TEST : 정상 FLOW
     */
    @Test
    fun all(){
        println("all");
        sprinklePost("", 100000,5); // 뿌리기 API
        Thread.sleep(300)
        sprinkleGet(); // 조회 API
        receivePost(""); // 받기 API, uerId = "" 이면 신규가입
        //receivePost(""); // 받기 API, uerId = "" 이면 신규가입
        sprinkleGet(); // 조회 API
    }

    /**
     * 전체 API TEST : 동시 접속 FLOW
     */
    @Test
    fun allParallel(){
        println("allParallel");
        val userCnt = 5;
        sprinklePost("", 100000,userCnt);  // 뿌리기 API
        Thread.sleep(300)
        IntStream.range(0, userCnt + 3).parallel().forEach { index: Int ->
            println("Starting " + Thread.currentThread().name + ", index=" + index + ", " + LocalDateTime.now())
            try {
                receivePost(""); // 받기 API, uerId = "" 이면 신규가입
            } catch (e: InterruptedException) {
            }
        }

        sprinkleGet(); // 조회 API
    }

    /** 뿌리기 API TEST
     * @sample onSuccess
     * -- [sprinklePost] req :
     * -- [sprinklePost] res.headers=X-USER-ID:125, X-ROOM-ID:RXP
       -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T01:58:30.002837","data":"RXP"}
     */
    @Test
    fun sprinklePostTest() {
        sprinklePost("", 1000,10);
    }

    /** 뿌리기 조회 API TEST
     * @sample onSuccess
    -- [sprinkleGet] req : userId=125, roomId=RXP
    -- [sprinkleGet] res.headers=X-USER-ID:125, X-ROOM-ID:RXP
    -- [sprinkleGet] res.body={"result":"SUCCESS","serverTime":"2020-06-27T02:07:44.401068","data":{"roomMakeDate":"2020-06-27T01:58:30","roomMoney":1000,"userTotalMoney":379,"userList":[{"userMoney":377,"userId":126},{"userMoney":2,"userId":127}]}}
     */
    @Test
    fun sprinkleGetTest() {
        sprinkleGet("125", "RXP");
    }

    /** 받기 API TEST
     * @sample onSuccess
    -- [receivePost] req : userId=, roomId=RXP
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:RXP
    -- [receivePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T02:07:23.834838","data":"2 원"}
     */
    @Test
    fun receivePostTest() {
        receivePost("", "RXP");
    }
}
