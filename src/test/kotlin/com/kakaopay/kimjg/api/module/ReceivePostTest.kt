package com.kakaopay.kimjg.api.module

import com.kakaopay.kimjg.api.common.BaseTest
import com.kakaopay.kimjg.api.common.message.Const
import org.junit.jupiter.api.Test
import org.springframework.http.*
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

/**
 * 받기 API TEST
 * URI : /api/v1.0/user/receive
 * Content-Type : application/x-www-form-urlencoded
 * Accept : application/json
 * Method : POST
 */
class ReceivePostTest : BaseTest() {
    /**
     * @sample onSuccess
     * 뿌리기 후,
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:203, X-ROOM-ID:SQF
    -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:03:50.487787","data":"SQF"}
     * 받기
    -- [receivePost] req : userId=, roomId=SQF
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:SQF
    -- [receivePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:03:50.890252","data":"15818 원"}
     */
    @Test
    fun receivePostSuccess() {
        sprinklePost("", 100000,5);
        Thread.sleep(300)
        receivePost("");
    }

    /**
     * @sample Error : 사용자ID 형식에 맞지 않았을 때,
    -- [receivePost] req : userId=aaa, roomId=XXX
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:12:06.162633","data":"잘못된 사용자 요청 입니다. (100)"}
     */
    @Test
    fun receivePostIsErrorUserIdWrongFormat(){
        receivePost("aaa", "XXX");
    }

    /**
     * @sample Error : 사용자ID 형식에 맞지 않았을 때,
    -- [receivePost] req : userId=99999999, roomId=XXX
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:15:28.356052","data":"잘못된 사용자 요청 입니다. (101)"}
     */
    @Test
    fun receivePostIsErrorUserIdNotFound(){
        receivePost("99999999", "XXX");
    }

     /**
     * @sample Error : 필수값 오류 일 때, 대화방
     -- [receivePost] req : userId=, roomId=
     -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
     -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:14:21.961434","data":"대화방 정보가 없습니다. (200)"}
     */
    @Test
    fun receivePostIsErrorRoomIdIsNull(){
         receivePost("", "");
    }

    /**
     * @sample Error : 대화방 소유주 요청시,
    -- [receivePost] req : userId=211, roomId=JUM
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:18:09.96344","data":"소유자는 참여 할 수 없습니다. (302)"}
     */
    @Test
    fun receivePostRoomOwnRequest(){
        sprinklePost("", 100000,5);
        Thread.sleep(300)
        receivePost();
    }

    /**
     * @sample Error : 중복참여,
     * 뿌리기 후,
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:218, X-ROOM-ID:BVT
    -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:26:40.256","data":"BVT"}
     * 받기 1,
    -- [receivePost] req : userId=, roomId=BVT
    -- [receivePost] res.headers=X-USER-ID:219, X-ROOM-ID:BVT
    -- [receivePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:26:40.876023","data":"11045 원"}
     * 받기 2,
    -- [receivePost] req : userId=219, roomId=BVT
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:26:41.040087","data":"이미 참여 하였습니다. (305)"}
     */
    @Test
    fun receivePostDuplicateParticipation(){
        sprinklePost("", 100000,5);
        Thread.sleep(300)
        receivePost("", sprinkleRoomId, true);
        receivePost(sprinkleUserId,sprinkleRoomId,true);
    }

    /**
     * @sample Error : 이벤트 종료,
     * 뿌리기 후,
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:222, X-ROOM-ID:UVV
    -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:29:22.507044","data":"UVV"}
     * 받기 1,
    -- [receivePost] req : userId=, roomId=UVV
    -- [receivePost] res.headers=X-USER-ID:223, X-ROOM-ID:UVV
    -- [receivePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:29:22.859702","data":"100000 원"}
     * 받기 2,
    -- [receivePost] req : userId=, roomId=UVV
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:29:22.919507","data":"뿌리기 이벤트가 종료되었습니다. (306)"}
     */
    @Test
    fun receivePostEventEnd(){
        sprinklePost("", 100000,1);
        Thread.sleep(300)
        receivePost("");
        receivePost("");
    }

    /**
     * @sample Error : 종료된 대화방 조회 시,
     * 뿌리기 후,
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:225, X-ROOM-ID:TWB
    -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:30:53.421005","data":"TWB"
     * 받기 1,
    -- [receivePost] req : userId=, roomId=TWB
    -- [receivePost] res.headers=X-USER-ID:226, X-ROOM-ID:TWB
    -- [receivePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T03:30:53.78115","data":"100000 원"}
     * 받기 2,
    -- [receivePost] req : userId=, roomId=TWB
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:30:53.847817","data":"뿌리기 이벤트가 종료되었습니다. (306)"}
     * 받기 3,
    -- [receivePost] req : userId=, roomId=TWB
    -- [receivePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [receivePost] res.body={"result":"FAIL","serverTime":"2020-06-27T03:30:53.872113","data":"종료 된 대화방 입니다. (303)"}
     */
    @Test
    fun receivePostEventEndEnd(){
        sprinklePost("", 100000,1);
        Thread.sleep(300)
        receivePost("");
        receivePost("");
        receivePost("");
    }
}
