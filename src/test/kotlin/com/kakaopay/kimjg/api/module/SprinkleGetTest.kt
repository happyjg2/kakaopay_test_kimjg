package com.kakaopay.kimjg.api.module

import com.kakaopay.kimjg.api.common.BaseTest
import org.junit.jupiter.api.Test

/**
 * 뿌리기 조회 API TEST
 * URI : /api/v1.0/user/sprinkle
 * Content-Type : application/x-www-form-urlencoded
 * Accept : application/json
 * Method : GET
 */
class SprinkleGetTest : BaseTest() {
    /**
     * @sample onSuccess
     * 뿌리기 후,
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:201, X-ROOM-ID:UYR
    -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T02:37:51.194785","data":"UYR"}
     * 조회
    -- [sprinkleGet] req : userId=201, roomId=UYR
    -- [sprinkleGet] res.headers=X-USER-ID:201, X-ROOM-ID:UYR
    -- [sprinkleGet] res.body={"result":"SUCCESS","serverTime":"2020-06-27T02:37:51.548193","data":{"roomMakeDate":"2020-06-27T02:37:51","roomMoney":100000,"userTotalMoney":0,"userList":[]}}
     */
    @Test
    fun sprinkleGetSuccess() {
        sprinklePost("", 100000,5);
        Thread.sleep(300)
        sprinkleGet();
    }

    /**
     * @sample Error : 사용자ID 형식에 맞지 않았을 때,
    -- [sprinkleGet] req : userId=aaa, roomId=
    -- [sprinkleGet] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinkleGet] res.body={"result":"FAIL","serverTime":"2020-06-27T02:48:15.496132","data":"잘못된 사용자 요청 입니다. (100)"}
     */
    @Test
    fun sprinkleGetIsErrorUserIdWrongFormat(){
        sprinkleGet("aaa", "");
    }

    /**
     * @sample Error : 필수값 오류 일 때, 사용자 ID
    -- [sprinkleGet] req : userId=, roomId=
    -- [sprinkleGet] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinkleGet] res.body={"result":"FAIL","serverTime":"2020-06-27T02:49:08.256977","data":"사용자 정보가 없습니다. (103)"}
     */
    @Test
    fun sprinkleGetIsErrorUserIdIsNull(){
        sprinkleGet("", "");
    }

    /**
     * @sample Error : 필수값 오류 일 때, 대화방
    -- [sprinkleGet] req : userId=121, roomId=
    -- [sprinkleGet] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinkleGet] res.body={"result":"FAIL","serverTime":"2020-06-27T02:50:57.402817","data":"대화방 정보가 없습니다. (200)"}
     */
    @Test
    fun sprinkleGetIsErrorRoomIdIsNull(){
        sprinkleGet("121", "");
    }

    /**
     * @sample Error : 소유자가 아닌 대화방 조회시,
    -- [sprinkleGet] req : userId=121, roomId=ABA
    -- [sprinkleGet] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinkleGet] res.body={"result":"FAIL","serverTime":"2020-06-27T02:55:22.739376","data":"요청한 사용자의 대화방 정보가 없습니다. (201)"}
     */
    @Test
    fun sprinkleWrongSearch(){
        sprinkleGet("121", "ABA");
    }
}
