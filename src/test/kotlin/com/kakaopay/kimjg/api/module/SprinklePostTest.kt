package com.kakaopay.kimjg.api.module

import com.kakaopay.kimjg.api.common.BaseTest
import org.junit.jupiter.api.Test

/**
 * 뿌리기 API TEST
 * URI : /api/v1.0/user/sprinkle
 * Content-Type : application/x-www-form-urlencoded
 * Accept : application/json
 * Method : POST
 */
class SprinklePostTest : BaseTest() {
    /**
     * @sample onSuccess
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:131, X-ROOM-ID:NEN
    -- [sprinklePost] res.body={"result":"SUCCESS","serverTime":"2020-06-27T02:09:33.82353","data":"NEN"}
     */
    @Test
    fun sprinklePostSuccess() {
        sprinklePost("", 1000,10);
    }

    /**
     * @sample Error : 사용자ID 형식에 맞지 않았을 때,
    -- [sprinklePost] req : aaa
    -- [sprinklePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinklePost] res.body={"result":"FAIL","serverTime":"2020-06-27T02:13:41.962235","data":"잘못된 사용자 요청 입니다. (100)"}
     */
    @Test
    fun sprinklePostIsErrorUserIdWrongFormat(){
        sprinklePost("aaa", 1000,10);
    }

    /**
     * @sample Error : 존재하지 않은 사용자ID 조회시,
    -- [sprinklePost] req : 99999999
    -- [sprinklePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinklePost] res.body={"result":"FAIL","serverTime":"2020-06-27T02:16:17.438363","data":"잘못된 사용자 요청 입니다. (101)"}
     */
    @Test
    fun sprinklePostIsErrorUserIdNotFound(){
        sprinklePost("99999999", 1000,10);
    }

    /** @sample Error : 필수값 오류 일 때, 뿌리기 금액
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinklePost] res.body={"result":"FAIL","serverTime":"2020-06-27T02:31:51.739187","data":"뿌리기 금액 요청값이 없습니다. (401)"}
     */
    @Test
    fun sprinklePostIsErrorMoneyIsNull(){
        sprinklePost("", null,10);
    }

    /** @sample Error : 필수값 오류 일 때, 뿌리기 인원
    -- [sprinklePost] req :
    -- [sprinklePost] res.headers=X-USER-ID:, X-ROOM-ID:
    -- [sprinklePost] res.body={"result":"FAIL","serverTime":"2020-06-27T02:32:21.926106","data":"뿌리기 인원 요청값이 없습니다. (402)"}
     */
    @Test
    fun sprinklePostIsErrorUserCntIsNull(){
        sprinklePost("", 10000,null);
    }
}
