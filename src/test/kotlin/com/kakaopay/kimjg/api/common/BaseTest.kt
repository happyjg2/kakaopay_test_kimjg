package com.kakaopay.kimjg.api.common

import com.kakaopay.kimjg.api.common.message.Const
import org.springframework.http.*
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

/**
 * 기본 Test 환경 설정
 *
 * sprinklePost : 요청 시 헤더의 사용자식별값, 룸식별값이 저장되어서 같은 Flow에 받기API, 조회API 호출 시 처리를 할 수 있다.
 */
open class BaseTest {
    var URL = "http://localhost:8080";
    var sprinkleUserId = "";
    var sprinkleRoomId = "";
    var index = 0;

    /**
     * 헤더 설정
     */
    fun setHeader(userId:String? = null, roomId: String? = null): HttpHeaders {
        val headers = HttpHeaders()
        val mediaType = MediaType("application", "json", StandardCharsets.UTF_8)
        headers.contentType = MediaType.APPLICATION_FORM_URLENCODED
        headers["Accept"] = MediaType.APPLICATION_JSON.toString()
        headers["Accept-Charset"] = Charset.forName("UTF-8").toString();
        if(!userId.isNullOrEmpty()){
            headers[Const.USER_ID] = userId;
        }

        if(!roomId.isNullOrEmpty()){
            headers[Const.ROOM_ID] = roomId;
        }

        return headers;
    }

    /**
     * 1. 뿌리기 API TEST
     */
    fun sprinklePost(userId:String? = "", money: Int? = 0, userCnt: Int? = 0) {
        var index = this.index++;
        val param: MultiValueMap<String, Any> = LinkedMultiValueMap()
        param["money"] = money;
        param["userCnt"] = userCnt;
        val entity: HttpEntity<*> = HttpEntity<Any?>(param, setHeader(userId))

        var restTemplate = RestTemplate();
        restTemplate.getMessageConverters().add(0, StringHttpMessageConverter(Charset.forName("UTF-8")))
        val res: ResponseEntity<String> = restTemplate.postForEntity("$URL/api/v1.0/user/sprinkle", entity, String::class.java)

        println("-- -----------------------------------------")
        println("-- [sprinklePost-$index] req : $userId")
        println("-- [sprinklePost-$index] res.headers=X-USER-ID:" + (res.headers[Const.USER_ID]?.first() ?: "")  + ", X-ROOM-ID:"+ (res.headers[Const.ROOM_ID]?.first() ?: ""));
        println("-- [sprinklePost-$index] res.body=" + res.body);
        println("-- -----------------------------------------")

        sprinkleUserId = res.headers[Const.USER_ID]?.first() ?: ""
        sprinkleRoomId = res.headers[Const.ROOM_ID]?.first() ?: ""
    }

    /**
     * 조회 API TEST
     */
    fun sprinkleGet(userId:String? = sprinkleUserId, roomId: String? = sprinkleRoomId) {
        var index = this.index++;
        val param = mutableMapOf<String, Any?>();

        val entity: HttpEntity<*> = HttpEntity<Any?>(setHeader(userId, roomId))
        var restTemplate = RestTemplate();
        val res: ResponseEntity<String> = restTemplate.exchange("$URL/api/v1.0/user/sprinkle", HttpMethod.GET, entity, String::class.java, param)

        println("-- -----------------------------------------")
        println("-- [sprinkleGet-$index] req : userId=$userId, roomId=$roomId")
        println("-- [sprinkleGet-$index] res.headers=X-USER-ID:" + (res.headers[Const.USER_ID]?.first() ?: "")  + ", X-ROOM-ID:"+ (res.headers[Const.ROOM_ID]?.first() ?: ""));
        println("-- [sprinkleGet-$index] res.body=" + res.body);
        println("-- -----------------------------------------")
    }

    /**
     * 받기 API TEST
     */
    fun receivePost(userId:String? = sprinkleUserId, roomId: String? = sprinkleRoomId, isDuplicateCheck:Boolean? = false){
        var index = this.index++;
        val param: MultiValueMap<String, Any> = LinkedMultiValueMap()

        val entity: HttpEntity<*> = HttpEntity<Any?>(param, setHeader(userId, roomId))
        var restTemplate = RestTemplate();
        restTemplate.getMessageConverters().add(0, StringHttpMessageConverter(Charset.forName("UTF-8")))
        val res: ResponseEntity<String> = restTemplate.postForEntity("$URL/api/v1.0/user/receive", entity, String::class.java)

        println("-- -----------------------------------------")
        println("-- [receivePost-$index] req : userId=$userId, roomId=$roomId")
        println("-- [receivePost-$index] res.headers=X-USER-ID:" + (res.headers[Const.USER_ID]?.first() ?: "")  + ", X-ROOM-ID:"+ (res.headers[Const.ROOM_ID]?.first() ?: ""));
        println("-- [receivePost-$index] res.body=" + res.body);
        println("-- -----------------------------------------")

        if(isDuplicateCheck!!) {
            sprinkleUserId = res.headers[Const.USER_ID]?.first() ?: ""
            sprinkleRoomId = res.headers[Const.ROOM_ID]?.first() ?: ""
        }
    }
}
