<h1>API</h1>
<hr/>
    <ul>
        <li>목차
            <ul>
                <li><a href="./README.md">소개</a></li>
                <li><a href="./README_project.md">프로젝트 구조 및 구성</a></li>
                <li><a href="./README_interface.md">인터페이스 정의서</a></li>
                <li><a href="./README_table.md">테이블 정의서</a></li>
            </ul>
         </li>
    </ul>
<hr/>

<h2>인터페이스 정의서</h2>

<h3>시스템 구성도</h3>
<ul>
    <li>
        <h4>Architecture</h4>
        Client <-> API Server <-> RDBMS
        <ul>
            <li>API Server : 뿌리기 기능을 이용하기 위해 Client와 G/W 간의 통신</li>
        </ul> 
    </li>
</ul>

<h3>프로토콜 구성도</h3>
<blockquote>
API 서버의 Protocol은 HTTP를 이용한다. 서버는 Rest API로 구성되어 있다.<br/>
Client는 Request 정보를 각 API의 호출 URI로 Http Paramter 형태로 요청한다. <br/>
Server는 Response 정보를 Http Body에 Json으로 구성하여 응답한다.<br/>
</blockquote>
<ul>
    <li>
        <h4>Http Header</h4>
            ○ 요청한 사용자의 식별값은 숫자 형태이며 "X-USER-ID" 라는 HTTP Header로 전달됩니다.<br/>
            ○ 요청한 사용자가 속한 대화방의 식별값은 문자 형태이며 "X-ROOM-ID" 라는 HTTP Header로 전달됩니다.<br/>
            ○ Client는 서버에서 받은 사용자 식별값과 대화방 식별값 정보를 다음 요청시에 HTTP Header로 재 전달 해야한다.<br/>
            ○ 사용자 식별값이 없으면, 신규 가입 대상자라 판단한다.
        <table border="1">
        <tr>
            <th>Element</th>
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
        <tr>
            <td>X-USER-ID</td>
            <td>사용자 식별값</td>
            <td>숫자</td>
            <td></td>
        </tr>
        <tr>
            <td>X-ROOM-ID</td>
            <td>대화방 식별값</td>
            <td>문자리 3자리</td>
            <td></td>
        </tr>
        </table>
    </li>
    <li>
        <h4>Http Request</h4>
            ○ 각 API 호출 시 key=value&key=value 형태의 parameter로 호출 한다. 
    </li>
    <li>
        <h4>Http Response</h4>
            <table border="1">
                <tr>
                    <th>Element</th>
                    <th>Name</th>
                    <th>필수항목</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>result</td>
                    <td>응답 결과</td>
                    <td>M</td>
                    <td>성공 : SUCCESS, 실패 : FAIL</td>
                </tr>
                <tr>
                    <td>serverTime</td>
                    <td>서버 응답 시간</td>
                    <td>M</td>
                    <td></td>
                </tr>
                <tr>
                    <td>data</td>
                    <td>각 API 결과</td>
                    <td>O</td>
                    <td>에러메시지 포함</td>
                </tr>
            </table>
            <h5>Sample</h5>
            <pre>
{
    "result":"SUCCESS",
     "serverTime":"2020-06-27T02:37:51.548193",
     "data":
        {
        "roomMakeDate":"2020-06-27T02:37:51",
        "roomMoney":100000,
        "userTotalMoney":0,
        "userList":[]
        }
}
            </pre>   
    </li>
</ul>

<h3>API 정의</h3>
<blockquote>
API는 RestAPI로 구성되어 있고, 뿌리기 / 조회 / 받기 API 3가지로 구성되어 있다.
</blockquote>
<table border="1">
    <tr>
        <th>이름</th>
        <th>URI</th>
        <th>Method</th>
        <th>기능</th>
    </tr>
    <tr>
        <td>뿌리기 API</td>
        <td>/api/v1.0/user/sprinkle</td>
        <td>POST</td>
        <td>사용자의 뿌리기 방 생성 API</td>
    </tr>
    <tr>
        <td>조회 API</td>
        <td>/api/v1.0/user/sprinkle</td>
        <td>GET</td>
        <td>뿌리기 방 조회 및 현황 내역 API</td>
    </tr>
    <tr>
        <td>받기 API</td>
        <td>/api/v1.0/user/sprinkle</td>
        <td>POST</td>
        <td>뿌리기 방에서 받기 API</td>
    </tr>
</table>

<h3>API 상세 정의</h3>
<blockquote>
Response는 Http Body의 data Element에 대해서만 정의한다.
</blockquote>
<ul>
    <li>
        <h4>뿌리기 API</h4>
        의
        <table border="1">
        <tr>
            <td>URI</td>
            <td>/api/v1.0/user/sprinkle</td>
        </tr>
        <tr>
            <td>Method</td>
            <td>POST</td>
        </tr>
        <tr>
            <td>Content-Type</td>
            <td>application/x-www-form-urlencoded</td>
        </tr>
        <tr>
            <td>Accept</td>
            <td>application/json</td>
        </tr>
        </table>
        ○ Request Parameter
        <table border="1">
            <tr>
                <th>Parameter</th>
                <th>필수여부</th>
                <th>설명</th>
            </tr>
            <tr>
                <td>money</td>
                <td>M</td>
                <td>뿌리기 금액</td>
            </tr>
            <tr>
                <td>userCnt</td>
                <td>M</td>
                <td>뿌리기 인원 수</td>
            </tr>
        </table>
        ○ Response 
        <table border="1">
            <tr>
                <th>Element</th>
                <th>설명</th>
            </tr>
            <tr>
                <td>body</td>
                <td>뿌리기 방 식별값</td>
            </tr>
        </table>
         ○ Sample 
        <pre>
{
    "result":"SUCCESS",
    "serverTime":"2020-06-27T01:58:30.002837",
    "data":"RXP"
}
        </pre>
    </li>
    <li>
        <h4>조회 API</h4>
        ○ Request<br/>
        <table border="1">
        <tr>
            <td>URI</td>
            <td>/api/v1.0/user/sprinkle</td>
        </tr>
        <tr>
            <td>Method</td>
            <td>GET</td>
        </tr>
        <tr>
            <td>Content-Type</td>
            <td>application/x-www-form-urlencoded</td>
        </tr>
        <tr>
            <td>Accept</td>
            <td>application/json</td>
        </tr>
        </table>
        ○ Request Parameter
        <table border="1">
            <tr>
                <th>Parameter</th>
                <th>필수여부</th>
                <th>설명</th>
            </tr>
        </table>
        ○ Response 
        <table border="1">
            <tr>
                <th>Element</th>
                <th>설명</th>
                <th></th>
            </tr>
            <tr>
                <td>body</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>roomMakeDate</td>
                <td>뿌린시각</td>
            </tr>
            <tr>
                <td></td>
                <td>roomMoney</td>
                <td>뿌린금액</td>
            </tr>
            <tr>
                <td></td>
                <td>userTotalMoney</td>
                <td>받기완료된금액</td>
            </tr>
            <tr>
                <td></td>
                <td>userList</td>
                <td>받기완료된정보 목록<br/>
                    <table>
                    <tr>
                        <td>userMoney</td>
                        <td>받은금액</td>
                    </tr>
                     <tr>
                        <td>userId</td>
                        <td>받은 사용자 아이디</td>
                    </tr>
                    </table>
                </td>
            </tr>
        </table>
        ○ Sample 
<pre>
{
    "result":"SUCCESS",
    "serverTime":"2020-06-27T05:15:18.076634",
    "data":
        {
            "roomMakeDate":"2020-06-27T01:58:30",
            "roomMoney":1000,
            "userTotalMoney":384,
            "userList":
                        [
                            {
                                "userMoney":377,
                                "userId":126
                             },
                             {
                                "userMoney":2,
                                "userId":127
                             },
                             {
                                "userMoney":2,
                                "userId":129
                             },
                             {
                                "userMoney":3,
                                "userId":130
                              }
                         ]
         }
}
</pre>
    </li>
    <li>
            <h4>받기 API</h4>
            ○ Request<br/>
            <table border="1">
            <tr>
                <td>URI</td>
                <td>/api/v1.0/user/receive</td>
            </tr>
            <tr>
                <td>Method</td>
                <td>POST</td>
            </tr>
            <tr>
                <td>Content-Type</td>
                <td>application/x-www-form-urlencoded</td>
            </tr>
            <tr>
                <td>Accept</td>
                <td>application/json</td>
            </tr>
            </table>
            ○ Request Parameter
            <table border="1">
                <tr>
                    <th>Parameter</th>
                    <th>필수여부</th>
                    <th>설명</th>
                </tr>
            </table>
            ○ Response 
            <table border="1">
                <tr>
                    <th>Element</th>
                    <th>설명</th>
                </tr>
                <tr>
                    <td>body</td>
                    <td>받은 금액</td>
                </tr>
            </table>
             ○ Sample 
            <pre>
{
    "result":"SUCCESS",
    "serverTime":"2020-06-27T02:07:23.834838",
    "data":"500 원"
}
            </pre>
        </li>
</ul>

<h3>에러 정의</h3>
<blockquote>
비즈니스에러 및 시스템 에러는 Response.Status = 200으로 내려 주고, 에러 결과를 HTTP Response 응답 규격으로 내려준다.<br/>
Response Data의 'result'가 FAIL이 되고, 메세지는 'data' 엘리먼트로 내려준다.
</blockquote>
<ul>
    <li>
    <h4>에러 코드 정보 </h4>
    <table border="1">
        <tr><th>Error Code</th><th>Error Meesage</th></tr>
        <tr><td>100</td><td>잘못된 사용자 요청 입니다."</td></tr>
        <tr><td>101</td><td>잘못된 사용자 요청 입니다."</td></tr>
        <tr><td>103</td><td>사용자 정보가 없습니다."</td></tr>
        <tr><td>200</td><td>대화방 정보가 없습니다."</td></tr>
        <tr><td>201</td><td>요청한 사용자의 대화방 정보가 없습니다."</td></tr>
        <tr><td>202</td><td>조회 시간이 지났습니다."</td></tr>
        <tr><td>301</td><td>잘못된 대화방 요청 입니다."</td></tr>
        <tr><td>302</td><td>소유자는 참여 할 수 없습니다."</td></tr>
        <tr><td>303</td><td>종료 된 대화방 입니다."</td></tr>
        <tr><td>304</td><td>참여 시간이 종료되었습니다."</td></tr>
        <tr><td>305</td><td>이미 참여 하였습니다."</td></tr>
        <tr><td>306</td><td>뿌리기 이벤트가 종료되었습니다."</td></tr>
        <tr><td>307</td><td>사용자가 많습니다. 잠시 후 다시 시도해 주세요."</td></tr>
        <tr><td>401</td><td>뿌리기 금액 요청값이 없습니다."</td></tr>
        <tr><td>402</td><td>뿌리기 인원 요청값이 없습니다."</td></tr>
        <tr><td>403</td><td>뿌리기 금액은 뿌리기 인원보다 많아야 합니다.</td></tr>
        <tr><td>500</td><td>대화방을 생성 할 수 없습니다. 문제가 지속되면 관리자에게 문의 바랍니다.</td></tr>
    </table>
    ○ sample
<pre>
{
    "result":"FAIL",
    "serverTime":"2020-06-27T02:50:57.402817",
    "data":"대화방 정보가 없습니다. (200)"
}
</pre>
    </li>
</ul>

