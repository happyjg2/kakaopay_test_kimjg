<h1>API</h1>
<hr/>
    <ul>
        <li>목차
            <ul>
                <li><a href="./README.md">소개</a></li>
                <li><a href="./README_project.md">프로젝트 구조 및 구성</a></li>
                <li><a href="./README_interface.md">인터페이스 정의서</a></li>
                <li><a href="./README_table.md">테이블 정의서</a></li>
            </ul>
         </li>
    </ul>
<hr/>

<h2>소개</h2>
뿌리기 기능 구현하기 프로젝트

<h2>요구사항</h2>
카카오페이에는 머니 뿌리기 기능이 있습니다.
<ul>
    <li>사용자는 다수의 친구들이 있는 대화방에서 뿌릴 금액과 받아갈 대상의 숫자를 입력하여 뿌리기 요청을 보낼 수 있습니다.</li>
    <li>요청 시 자신의 잔액이 감소되고 대화방에는 뿌리기 메세지가 발송됩니다.</li>
    <li>대화방에 있는 다른 사용자들은 위에 발송된 메세지를 클릭하여 금액을 무작위로 받아가게 됩니다.</li>
</ul>

<h3>핵심 문제해결 전략</h3>
<ul>
    <li>뿌리기 방 식별값은 랠덤으로 생성 된다.</li>
    <li>뿌리기 방 식별값이 PK로 되어 있어, 유니크한 값을 갖는다.</li>
    <li>동시접속 시 받기 기능이 정상이어야 한다.</li>
    <li>받기 API 마감 시 뿌리기방정보 table(row 수가 적음)에 구분값을 지정하여, 참여정보 table(row 수가 많음) 진입을 차단하여 속도를 향상 시켰다.</li>
</ul>

<h3>테스트 결과서</h3>
<ul>
    <li>뿌리기 API : <a href="./src/test/kotlin/com/kakaopay/kimjg/api/module/SprinklePostTest.kt">SprinklePostTest</a> 참조</li>
    <li>조회 API : <a href="./src/test/kotlin/com/kakaopay/kimjg/api/module/SprinkleGetTest.kt">SprinkleGetTest</a> 참조</li>
    <li>받기 API : <a href="./src/test/kotlin/com/kakaopay/kimjg/api/module/ReceivePostTest.kt">ReceivePostTest</a> 참조</li>
    <li>전체 API : <a href="./src/test/kotlin/com/kakaopay/kimjg/api/module/SprinkleAllTest.kt">SprinkleAllTest</a> 참조</li>
</ul>

<h3>Query Plan</h3>
<pre>
mysql> EXPLAIN  select * from sprinkle_user where user_id=354;
+----+-------------+---------------+------------+-------+---------------+---------+---------+-------+------+----------+-------+
| id | select_type | table         | partitions | type  | possible_keys | key     | key_len | ref   | rows | filtered | Extra |
+----+-------------+---------------+------------+-------+---------------+---------+---------+-------+------+----------+-------+
|  1 | SIMPLE      | sprinkle_user | NULL       | const | PRIMARY       | PRIMARY | 4       | const |    1 |   100.00 | NULL  |
+----+-------------+---------------+------------+-------+---------------+---------+---------+-------+------+----------+-------+
1 row in set, 1 warning (0.00 sec)

mysql> EXPLAIN
    -> select * from sprinkle_room where room_id='LEY' and user_id=3;
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+-----------------------------------------------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra                                               |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+-----------------------------------------------------+
|  1 | SIMPLE      | NULL  | NULL       | NULL | NULL          | NULL | NULL    | NULL | NULL |     NULL | Impossible WHERE noticed after reading const tables |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+-----------------------------------------------------+
1 row in set, 1 warning (0.00 sec)

mysql> EXPLAIN
    -> select * from sprinkle_room where room_id='LEY' and stop_yn='Y';
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+-----------------------------------------------------+
| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows | filtered | Extra                                               |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+-----------------------------------------------------+
|  1 | SIMPLE      | NULL  | NULL       | NULL | NULL          | NULL | NULL    | NULL | NULL |     NULL | Impossible WHERE noticed after reading const tables |
+----+-------------+-------+------------+------+---------------+------+---------+------+------+----------+-----------------------------------------------------+
1 row in set, 1 warning (0.00 sec)

mysql> EXPLAIN
    -> select * from sprinkle_participation where room_id='LEY' and user_id=354;
+----+-------------+------------------------+------------+------+-----------------+---------+---------+-------+------+----------+-------------+
| id | select_type | table                  | partitions | type | possible_keys   | key     | key_len | ref   | rows | filtered | Extra       |
+----+-------------+------------------------+------------+------+-----------------+---------+---------+-------+------+----------+-------------+
|  1 | SIMPLE      | sprinkle_participation | NULL       | ref  | room_id,user_id | user_id | 5       | const |    1 |     5.00 | Using where |
+----+-------------+------------------------+------------+------+-----------------+---------+---------+-------+------+----------+-------------+
1 row in set, 1 warning (0.00 sec)
</pre>

<h3>참고</h3>
<ul>
    <li>Kotlin, JPA 프로젝트 경험은 없지만 현 추세에 맞게 적용 해보았다.</li>
    <li>받기API 마감 시 NoSQL(Redis..) 등을 사용했으면 DB 부하율을 줄이고, 속도를 향상 시킬 수 있다.</li>
</ul>
