<h1>API</h1>
<hr/>
    <ul>
        <li>목차
            <ul>
                <li><a href="./README.md">소개</a></li>
                <li><a href="./README_project.md">프로젝트 구조 및 구성</a></li>
                <li><a href="./README_interface.md">인터페이스 정의서</a></li>
                <li><a href="./README_table.md">테이블 정의서</a></li>
            </ul>
         </li>
    </ul>
<hr/>

<h2>테이블 정의서</h2>

<h3>Table 정보</h3>
<ul>
    <li>
        <h4>뿌리기 회원정보</h4>
<pre>
-- 뿌리기 회원정보
CREATE TABLE sprinkle_user (
  user_id INT(11) NOT NULL AUTO_INCREMENT comment '사용자 구분값',
  assets INT(11) NOT NULL DEFAULT 999999999 comment '자산',
  create_date datetime NOT NULL comment '가입일',
  PRIMARY KEY (user_id)
) comment'뿌리기 회원정보';
</pre>
    </li>
    <li>
        <h4>뿌리기 대화방</h4>
<pre>
CREATE TABLE sprinkle_room (
  room_id VARCHAR(3) NOT NULL comment '대화방 token',
  room_money INT NOT NULL comment '뿌리기 금액',
  user_cnt INT NOT NULL comment '뿌리기 인원수',
  user_id int(11) NOT NULL comment '등록자',
  create_date datetime NOT NULL comment '생성일',
  stop_yn char(1) NOT NULL DEFAULT 'N' comment '대화방종료 YN',
  PRIMARY KEY (room_id),
  FOREIGN KEY (user_id) REFERENCES sprinkle_user (user_id)
) comment'뿌리기 대화방';
</pre>
    </li>
    <li>
        <h4>뿌리기 대화방 참여 정보</h4>
<pre>
-- 뿌리기 대화방 참여 정보
CREATE TABLE sprinkle_participation (
  seq int(11) NOT NULL AUTO_INCREMENT comment '참여정보 seq',
  room_id VARCHAR(3) NOT NULL comment '대화방 token',
  user_money INT NOT NULL comment '사용자별 할당 금액',
  user_id int(11) NULL comment '사용자',
  pp_date datetime NULL comment '참여날짜',
  PRIMARY KEY (seq),
  FOREIGN KEY (room_id) REFERENCES sprinkle_room (room_id),
  FOREIGN KEY (user_id) REFERENCES sprinkle_user (user_id)
) comment'뿌리기 대화방 참여 정보';
</pre>
    </li>
</ul>

<small>* 참고 : Index 확인</small>
<pre>
mysql> SHOW INDEX FROM sprinkle_user;
+---------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table         | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+---------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| sprinkle_user |          0 | PRIMARY  |            1 | user_id     | A         |          45 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
+---------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
1 row in set (0.00 sec)

mysql> SHOW INDEX FROM sprinkle_room;
+---------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table         | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+---------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| sprinkle_room |          0 | PRIMARY  |            1 | room_id     | A         |          17 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
| sprinkle_room |          1 | user_id  |            1 | user_id     | A         |          17 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
+---------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
2 rows in set (0.00 sec)

mysql> SHOW INDEX FROM sprinkle_participation;
+------------------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table                  | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+------------------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| sprinkle_participation |          0 | PRIMARY  |            1 | seq         | A         |          73 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
| sprinkle_participation |          1 | room_id  |            1 | room_id     | A         |          15 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
| sprinkle_participation |          1 | user_id  |            1 | user_id     | A         |          29 |     NULL |   NULL | YES  | BTREE      |         |               | YES     | NULL       |
+------------------------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
3 rows in set (0.00 sec)
</pre>
